---
layout: post
title: uefi相关内容笔记
categories: 学习
tags: Linux 启动 uefi bios 系统
---

一直希望能够弄懂uefi的相关内容, 对系统在引导启动时的行为做到心里有数.
简单地Google了一下, 感觉
[这一篇文章](https://www.happyassassin.net/2014/01/25/uefi-boot-how-does-that-actually-work-then/)
地味比较足, 打算先从这里入手.

## 准备知识

### GNU Parted

在命令行输入`sudo parted /dev/sda`进入GNU Parted命令行界面, `/dev/sda`是对应的磁盘文件.

使用`print`命令(可以简写成p)可以打印当前的分区信息, 我的分区信息是这样的:

```text
(parted) print
Model: ATA PLEXTOR PX-256M6 (scsi)
Disk /dev/sda: 256GB
Sector size (logical/physical): 512B/512B
Partition Table: gpt

Number  Start   End    Size    File system     Name                          Flags
 1      1049kB  316MB  315MB   ntfs            Basic data partition          hidden, diag
 2      316MB   420MB  105MB   fat32           EFI system partition          boot
 3      420MB   555MB  134MB                   Microsoft reserved partition  msftres
 4      555MB   212GB  212GB   ntfs            Basic data partition          msftdata
 6      212GB   249GB  36.7GB  ext4
 7      249GB   255GB  6252MB  linux-swap(v1)
 5      255GB   256GB  825MB   ntfs                                          hidden, diag
```

这块盘是新买的纯净安装的双系统~~然而msft还是把分区搞得那么污~~. 我对Flgas这个域不是很熟悉.
具体的资料在Parted的[手册](http://www.gnu.org/software/parted/manual/parted.html#set)里有. 
Flags主要是用来描述分区的功能和性质的.
