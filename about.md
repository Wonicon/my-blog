---
layout: page
title: 关于
permalink: /about.html
---

## 关于这里

这个水站是由[Jekyll](http://jekyllrb.com)生成的静态站点，使用略微修改的默认主题，托管在[Vultr](https://www.vultr.com/)的 VPS 上。

## 关于我

计算机专业本科大四在读的三无(没有竞赛经验，没有科研经验，没有工程经验)学生。